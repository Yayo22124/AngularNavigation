import { RouterModule, Routes } from '@angular/router';

import { AboutUsComponent } from './pages/about-us/about-us.component';
import { BlogComponent } from './pages/blog/blog.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { StoreComponent } from './pages/store/store.component';

const routes: Routes = [
  
  {
    path: 'Home',
    component: HomeComponent
  },
  {
    path: 'Store',
    component: StoreComponent
  },
  {
    path: 'Contact',
    component: ContactComponent
  },
  {
    path: 'About-us',
    component: AboutUsComponent
  },
  {
    path: 'Blog',
    component: BlogComponent
  },
  {
    path: "**",
    redirectTo: "Home"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
